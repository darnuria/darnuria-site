---
title: Accueil | darnuria.eu
layout: default.liquid
---

## Axel Viala / darnuria

Bienvenue sur mon site web personnel.

## En bref

Je développe des logiciels plutôt dans le domaine de la programmation système,
bas niveau voire jusque dans les kernels, néanmoins j'apprécie aussi le dev-web.

J'aime partager ce que je découvre et apprends.

## Contact

Il est possible de me contacter via XMPP ou par courriel

- Courriel: <a style="unicode-bidi:bidi-override; direction:rtl;">ue.airunrad@airunrad</a>
- XMPP: <a style="unicode-bidi:bidi-override; direction:rtl;">rf.evuamknil@airunrad</a>

## Liens un peu en vrac

J'ai une page de [liens](https://darnuria.eu/links) en vrac c'est parfois mis à jour.

## Réseaux et plateformes

Je suis actif sur les sites et réseaux suivants :

- [Github](https://github.com/darnuria)
- [StackOverflow](https://stackoverflow.com/users/2948508/darnuria)
- [Framagit](https://framagit.org/darnuria)
- [Mozillians](https://mozillians.org/u/darnuria/)
- [Framapiaf](https://framapiaf.org/@darnuria)

## Mes contributions et travaux

Je maintiens principalement cette liste pour me souvenir des projets auquels
j'ai contribué.

### Oressource

Je suis co-mainteneur de [Oressource](https://github.com/mart1ver/oressource) avec [Martin Vert](https://github.com/mart1ver) un
logiciel de gestion de flux de matières pour [ressourcerie](http://www.reemploi-idf.org/ressourceries-et-recycleries/),
j'ai principalement contribué à la remise sur pied du code.

N'Hésitez pas à contacter Martin ou moi pour des questions, bugs, fonctionnalités ou pour
supporter financièrement le projet.

Le logiciel est en licence [AGPL-v3](https://www.gnu.org/licenses/agpl-3.0.en.html).

### Bson crate

Modest contribution to the [binary json](https://en.wikipedia.org/wiki/BSON) crate [bson-crate](https://github.com/mongodb/bson-rust/), I
added build rule in `Cargo.toml` to exclude test files from the final crate. [Issue 202](https://github.com/mongodb/bson-rust/issues/202) [Pull-request #203](https://github.com/mongodb/bson-rust/pull/203)

Contribution made by using [cargo diet](https://github.com/the-lean-crate/cargo-diet).

### Rars

[Rars](https://github.com/TheThirdOne/rars) est un simulateur pédagogique pour processeur RiscV
Correction d'une erreur dans un exemple utilisant des timers en mode normal du processeur: [TheThirdOne/rars #79](https://github.com/TheThirdOne/rars/pull/79)

### [PeerTube](https://joinpeertube.org/)

Je me suis proposé pour résoudre l'issue [#189](https://github.com/Chocobozzz/PeerTube/issues/189)
qui proposait d'avoir une façon de donner un mot de passe par défaut au lancement.

J'ai donc ajouté une variable d'environnement comme proposé dans l'issue :

- [PR #1814](https://github.com/Chocobozzz/PeerTube/pull/1814) hash: [3daaa19](https://github.com/Chocobozzz/PeerTube/commit/3daaa1927474869f8dbaddd6b94b4c071e314e10)
- Documentation en ligne [MR: #16](https://framagit.org/framasoft/peertube/documentation/merge_requests/16)

### Converse.js

J'ai fait une contribution modeste au logiciel de messagerie XMPP en JavaScript
[converse.js](https://conversejs.org/), [False mentions positives in URLs and Email addresses](https://github.com/conversejs/converse.js/issues/1327) voici le commit en question: [Fix #1327: Refusing url and email as mentions](https://github.com/conversejs/converse.js/commit/b51d98d6d1d97df6f87143f82210380a5b192ee8) (hash: b51d98d6d1d97df6f87143f82210380a5b192ee8).

Le projet est très facile à mettre en place et utiliser. Les contributions sont
les bienvenues.

### Kernel de recherche Almos-mkh

En 2018 j'ai contribué au developpement du kernel [Almos-mkh](https://www-soc.lip6.fr/trac/almos-mkh) au cours d'une unité d'[initiation à la recherche](https://www-licence.ufr-info-p6.jussieu.fr:8083/lmd/licence/2017/ue/3I013-2018fev/) à l'université Sorbonne Université.
Adaptation de la NewlibC pour l'OS, fixer des bugs empêchant de finir une séquence de boot et plus.

Puis j'ai contribué au cours d'un stage d'été 2018 aussi, j'ai corrigé des bugs de synchronisation dans l'ordonnanceur,
dans le code d'initialisation du kernel et résolu de nombreux warning de compilation.

Je maintiens (parfois) un [mirroir git](https://framagit.org/almos-mkh/mirror-amk) de ce kernel, notament un [script d'installation](https://framagit.org/almos-mkh/internals/blob/master/setup.sh) communautaire.

### [Rust - libC](https://github.com/rust-lang/libc) raw binding

J'ai ajouté binding vers la libC pour les fonctions de manipulations de file descriptor relatifs:

- [PR 307](https://github.com/rust-lang/libc/pull/307) sha[26ee782](https://github.com/rust-lang/libc/pull/307/commits/26ee782523f2616568bbc30b545abd0f1122eefd)

### RushDroid : Un jeu Android pour la fac

[RushDroid](https://github.com/darnuria/Rushdroid) est un jeu Android minimaliste pour la fac
avec l'aide de [Corentin Ulliac](https://github.com/Finiganis), c'est volontairement épuré.

### 2014 - Rust - Tutorial

Contribution mineur au tutoriel en 2014 de rust [rust-lang/rust pr#12472](https://github.com/rust-lang/rust/pull/12472)

## Enseignements

### Cours à l'École Supérieur de Genie Informatique

(section en construction, liens à venir)

Je suis intervenant à l'ESGI depuis juin 2019 j'ai eu l'honneur de donner les cours suivants:

Sur l'année 2019 - 2020:

- Théorie et Pratique des système d'exploitation aux étudiant-e-s
de 2ème années en alternance [lien](https://darnuria.eu/2019-2020_os)

- Cours d'architecture des ordinateurs avec pour support le jeu d'instruction RiscV.

### Université de Marne la Vallée - UPEM

#### Master2 NUMI Cours de Python

De 2015 à 2017 en vacation, sur trois promotions du [master 2 NUMI](https://web.archive.org/web/20190512114008/https://webcache.googleusercontent.com/search?q=cache:zHdB3IAAF1oJ:https://shs.u-pem.fr/formations/master-etudes-numeriques-et-innovation-numi/+&cd=2&hl=en&ct=clnk&gl=fr),
j'ai donné un cours d'initiation par la pratique à Python à des étudiant-e-s en sociologie.
D'une année à l'autre, le cours s’étalait de 15 à 30h réparties sur un semestre.

Une partie des documents sont en ligne ici: <https://framagit.org/darnuria/NumiPython>

## Travail

Si vous souhaitez faire appel à mes services vous pouvez prendre contact avec [uptime-formation](https://uptime-formation.fr/) où je suis coopérateur ou m'envoyer
un courriel à `[ce domaine] chez [ce domaine] point eu`. Je fait partie de la coopérative d'activitée et d'emploi [Coopaname](https://www.coopaname.coop/).

## À propos de ce site web

Il est réalisé à l'aide de [Cobalt.rs](https://cobalt-org.github.io/) un
générateur de sites statique écrit en langage [Rust](https://www.rust-lang.org/).

### Mentions légales

La gestion informatique est réalisée par mes soins et l'hébergement par la société
[OVH SAS](https://www.ovh.com/).

Aucun cookie, outil publicitaire, pisteur n'est employé sur ce site.
